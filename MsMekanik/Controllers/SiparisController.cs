﻿using MsMekanik.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MsMekanik.Controllers
{
    
    public class SiparisController : Controller
    {
        msmekani_veriEntities baglanti = new msmekani_veriEntities();

        public ActionResult Index()
        {
            SiparisUrunList model = new SiparisUrunList();

            List<Urunler> urunList = baglanti.Urunler.OrderBy(x => x.UrunKodu).ToList();
            model.SiparisUrunListi = (from s in urunList
                                      select new SelectListItem
                                      {
                                          Text = s.UrunKodu,
                                          Value = s.ID.ToString()
                                      }).ToList();
            model.SiparisUrunListi.Insert(0, new SelectListItem { Value = "", Text = "Seçiniz", Selected = true });
            List<SiparisUrun> _SiparisList = new List<SiparisUrun>();
            var _model = new Siparis();
            for (int i = 0; i < 6; i++)
            {
                _model.SiparisUrun.Add(new SiparisUrun());
            }
            ViewBag.Urunler = new SelectList(baglanti.Urunler.ToList(), "ID", "UrunAdi");
            return View(_model);
        }

        [HttpPost]
        public ActionResult Index(Siparis data)
        {
            //todo : helperlar strongly type olmayacak
            if(data != null)
            {
                Siparis db_siparis = new Models.Siparis()
                {
                    TerminBaslangic=data.TerminBaslangic,
                    TerminBitis=data.TerminBitis,
                    Konu=data.Konu,
                    YetkiliAdi=data.YetkiliAdi,
                    FirmaAdi=data.FirmaAdi
                };
                foreach (var item in data.SiparisUrun)
                {
                    db_siparis.SiparisUrun.Add(new SiparisUrun()
                    {
                        Siparis=db_siparis,
                        Urunler=baglanti.Urunler.Find(item.Urunler.ID)
                    });
                }
                baglanti.Siparis.Add(db_siparis);
                baglanti.SaveChanges();   
            }


            //SiparisUrun siparisEkle = new SiparisUrun();


            //siparisEkle.Siparis.MusteriID = 1;

            //if (form["UrunKodu1"] != "" && form["UrunAdi1"] != "" && form["Adet1"] != "" && form["Malzeme1"] != "" && form["GerekliMalzeme1"] != "")
            //{
            //    siparisEkle.UrunID = Convert.ToInt32(form["UrunKodu1"]);
            //    siparisEkle.UrunID = 1;
            //    siparisEkle.Adedi= Convert.ToInt32(form["Adet1"]);
            //    siparisEkle.MalzemeMiktari= Convert.ToInt32(form["Malzeme1"]);
            //    baglanti.SiparisUrun.Add(siparisEkle);
            //    baglanti.SaveChanges();
            //}

            ViewBag.Urunler = new SelectList(baglanti.Urunler.ToList(), "ID", "UrunAdi");
            return View();
        }
        [HttpPost]
        public ActionResult AdiMalzemeListele(int id)
        {
            //List<Urunler> UrunlerListe = baglanti.Urunler.Where(x => x.ID == id).OrderBy(x => x.UrunAdi).ToList();

            //ViewBag.UrunList = new SelectList(UrunlerListe.ToList(), "ID", "UrunAdi");
            //List<SelectListItem> itemList = (from i in UrunlerListe
            //                                select new SelectListItem
            //                                {
            //                                    Value = i.ID.ToString(),
            //                                    Text=i.UrunAdi

            //                                }).ToList();
            
            var urun = baglanti.Urunler.FirstOrDefault(x => x.ID == id);
            
            return PartialView("_SecilenUrun",urun);
        }
    }
}