﻿using MsMekanik.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MsMekanik.Controllers
{
    public class DepoController : Controller
    {
        // GET: Depo
        msmekani_veriEntities baglanti = new msmekani_veriEntities();
        public ActionResult Index()
        {         
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            MalzemeGirisEnvanteri model = new MalzemeGirisEnvanteri();
            model.Tarih = DateTime.Now;
            model.MusteriAdi=form["MusteriAdi"].Trim();
            model.Tedarikci =form["Tedarikci"].Trim();
            model.MalzemeLotNo = form["MalzemeLotNo"].Trim();
            model.MalzmeCinsi = form["MalzemeCinsi"].Trim();
            model.MalzemeCapi =Convert.ToDouble( form["cap"].Trim());
            model.MalzemeBoyu = Convert.ToDouble(form["Boy"].Trim());
            model.MalzemeAdedi = Convert.ToInt32(form["Adet"].Trim());
            model.MalzemeTakipNo =form["MalzemeTakipNo"].Trim();
            model.RafNo = form["RafNo"].Trim();
            model.TeslimAlanID = 1;
            baglanti.MalzemeGirisEnvanteri.Add(model);
            baglanti.SaveChanges();
            return View();
        }
        public ActionResult Listeleme()
        {
            return View(baglanti.MalzemeGirisEnvanteri.ToList());
        }
        public ActionResult Delete(int id)
        {
            baglanti.MalzemeGirisEnvanteri.Remove(baglanti.MalzemeGirisEnvanteri.Find(id));
            baglanti.SaveChanges();
            return View("Listeleme",baglanti.MalzemeGirisEnvanteri);
        }
        public ActionResult Update(int id)
        {
            return View("Update", baglanti.MalzemeGirisEnvanteri.Find(id));
        }
        [HttpPost]
        public ActionResult Update(FormCollection form)
        {
            var mevcut = baglanti.MalzemeGirisEnvanteri.Find(form["ID"]);
            mevcut.MusteriAdi=form["MusteriAdi"].Trim();
            mevcut.Tedarikci= form["Tedarikci"].Trim();
            mevcut.MalzemeLotNo = form["MalzemeLotNo"].Trim();
            mevcut.MalzmeCinsi =  form["MalzemeCinsi"].Trim();
            mevcut.MalzemeCapi = Convert.ToDouble( form["cap"].Trim());
            mevcut.MalzemeBoyu =  Convert.ToDouble(form["Boy"].Trim());
            mevcut.MalzemeAdedi = Convert.ToInt32(form["Adet"].Trim());
            mevcut.MalzemeTakipNo =form["MalzemeTakipNo"].Trim();
            mevcut.RafNo =  form["RafNo"].Trim();
            mevcut.TeslimAlanID = 1;
            baglanti.MalzemeGirisEnvanteri.Add(mevcut);
            baglanti.SaveChanges();
            return View("Listeleme", baglanti.MalzemeGirisEnvanteri);
        }
    }
}