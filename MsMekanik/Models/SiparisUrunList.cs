﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MsMekanik.Models
{
    public class SiparisUrunList
    {
        public SiparisUrunList()
        {
            this.SiparisUrunAdiListi = new List<SelectListItem>();
            SiparisUrunAdiListi.Add(new SelectListItem { Text = "Seçiniz", Value = "" });
            this.SiparisMalzemeListi = new List<SelectListItem>();
            SiparisMalzemeListi.Add(new SelectListItem { Text = "Seçiniz", Value = "" });
        }
        
        public int UrunID { get; set; }

        public int UrunAdiID { get; set; }

        public int UrunMalzemeID { get; set; }

        public List<SelectListItem> SiparisUrunListi { get; set; }

        public List<SelectListItem> SiparisUrunAdiListi { get; set; }

        public List<SelectListItem> SiparisMalzemeListi { get; set; }
        


    }
}