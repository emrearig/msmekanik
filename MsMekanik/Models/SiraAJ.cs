//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MsMekanik.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SiraAJ
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SiraAJ()
        {
            this.Saatler = new HashSet<Saatler>();
        }
    
        public int ID { get; set; }
        public Nullable<int> Sira { get; set; }
        public Nullable<double> İstenenOlcu { get; set; }
        public Nullable<double> Tolerans { get; set; }
        public string KullanılanOlcuAleti { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Saatler> Saatler { get; set; }
    }
}
