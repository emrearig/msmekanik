//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MsMekanik.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class KaliteKontrolCati
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KaliteKontrolCati()
        {
            this.CatiTalasliImalat = new HashSet<CatiTalasliImalat>();
        }
    
        public int ID { get; set; }
        public Nullable<int> UrunID { get; set; }
        public Nullable<int> HazirlayanID { get; set; }
        public Nullable<int> OnaylayanID { get; set; }
    
        public virtual Calisan Calisan { get; set; }
        public virtual Calisan Calisan1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatiTalasliImalat> CatiTalasliImalat { get; set; }
    }
}
