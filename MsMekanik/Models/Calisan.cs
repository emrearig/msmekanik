//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MsMekanik.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Calisan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Calisan()
        {
            this.CatiIsEmri = new HashSet<CatiIsEmri>();
            this.CatiIsEmri1 = new HashSet<CatiIsEmri>();
            this.CatiIsEmri2 = new HashSet<CatiIsEmri>();
            this.CatiTalasliImalat = new HashSet<CatiTalasliImalat>();
            this.CatiTalasliImalat1 = new HashSet<CatiTalasliImalat>();
            this.DepoMalzemeGiris = new HashSet<DepoMalzemeGiris>();
            this.Formlar = new HashSet<Formlar>();
            this.Formlar1 = new HashSet<Formlar>();
            this.KaliteKontrolCati = new HashSet<KaliteKontrolCati>();
            this.KaliteKontrolCati1 = new HashSet<KaliteKontrolCati>();
            this.KontrolPlan = new HashSet<KontrolPlan>();
            this.MalzemeGirisEnvanteri = new HashSet<MalzemeGirisEnvanteri>();
            this.ProsesTakip = new HashSet<ProsesTakip>();
            this.Siparis = new HashSet<Siparis>();
            this.SonKaliteKontrol = new HashSet<SonKaliteKontrol>();
            this.SonKaliteKontrol1 = new HashSet<SonKaliteKontrol>();
            this.UrunTeslimFormu = new HashSet<UrunTeslimFormu>();
        }
    
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string TC { get; set; }
        public string Numara { get; set; }
        public string Adres { get; set; }
        public string KanGrubu { get; set; }
        public string Pozisyon { get; set; }
        public string Maas { get; set; }
        public string Aciklama { get; set; }
        public Nullable<System.DateTime> IseGiris { get; set; }
        public Nullable<System.DateTime> IstenAyrilma { get; set; }
        public Nullable<System.DateTime> GuncellemeTarih { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatiIsEmri> CatiIsEmri { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatiIsEmri> CatiIsEmri1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatiIsEmri> CatiIsEmri2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatiTalasliImalat> CatiTalasliImalat { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatiTalasliImalat> CatiTalasliImalat1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DepoMalzemeGiris> DepoMalzemeGiris { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formlar> Formlar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Formlar> Formlar1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KaliteKontrolCati> KaliteKontrolCati { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KaliteKontrolCati> KaliteKontrolCati1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KontrolPlan> KontrolPlan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MalzemeGirisEnvanteri> MalzemeGirisEnvanteri { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProsesTakip> ProsesTakip { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Siparis> Siparis { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SonKaliteKontrol> SonKaliteKontrol { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SonKaliteKontrol> SonKaliteKontrol1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UrunTeslimFormu> UrunTeslimFormu { get; set; }
    }
}
